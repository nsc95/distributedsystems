package edu.rit.cs;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;

public class Message implements Serializable {
    private int id = -1;
    private String type = "";
    private String IP = "";
    private int port = -1;
    private int from = -1;
    private File file = null;
    private int hashcode = -1;
    private String content = null;
    private String filename = null;

    public Message(String type){
        this.type = type;
    }
    public Message(int id,String type,String IP,int port,String content,String filename,int hashcode)
    {
        this.id = id;
        this.type = type;
        this.IP = IP;
        this.port = port;
        this.filename = filename;
        this.content = content;
        this.hashcode = hashcode;
    }
    public Message(int id,String type,String IP,int port,int from)
    {
        this.id = id;
        this.type = type;
        this.IP = IP;
        this.port = port;
        this.from = from;
    }

    public Message(String type,String IP,int port,String filename,String content)
    {
        this.type = type;
        this.IP = IP;
        this.port = port;
        this.filename = filename;
        this.content = content;
    }

    public Message(int id,String type,String IP,int port,int from,String filename,String content)
    {
        this.id = id;
        this.type = type;
        this.IP = IP;
        this.port = port;
        this.from = from;
        this.filename = filename;
        this.content = content;
    }

    public Message(String type,String IP,int port,int hashcode)
    {
        this.type = type;
        this.IP = IP;
        this.port = port;
        this.hashcode = hashcode;
    }

    public int getId()
    {
        return this.id;
    }
    public String getType(){
        return this.type;
    }
    public String getIP(){
        return this.IP;
    }
    public int getPort(){
        return this.port;
    }
    public int getFrom(){ return this.from; }
    public File getFile(){ return this.file; }
    public int getHashcode(){ return this.hashcode; }
    public String getContent(){return this.content;}
    public String getFilename() {return this.filename;}
}
