package edu.rit.cs;

import javax.xml.crypto.Data;
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class client {
    static Scanner in = new Scanner(System.in);
    static DatagramSocket sock;
    static InetAddress inetAddress;
    static String localIP;
    static Integer localPort;
    static {
        try {
            inetAddress = InetAddress.getLocalHost();
            localIP = inetAddress.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        sock = new DatagramSocket(Integer.parseInt(args[0]));
        localPort = Integer.parseInt(args[0]);
        System.out.println("1-push file\n 2-get file\n press any other key to exit");
        while (true) {
            boolean loop = true;
            switch (Integer.parseInt(in.nextLine())) {
                case 1:
                    System.out.println("enter filename");
                    String fname = in.nextLine();
                    System.out.println("enter file content");
                    String content = in.nextLine();
                    client.sendAndCreateFile(fname,content);
                    break;
                case 2:
                    System.out.println("enter the hashcode");
                    int hashcode = Integer.parseInt(in.nextLine());
                    getFile(hashcode);
                    break;
                default:
                    System.out.println("stop");
                    loop = false;
                    break;
            }
            if(!loop)
                break;
        }
        System.exit(-1);
    }

    public static void sendAndCreateFile(String fname,String content) throws IOException, ClassNotFoundException {
        File f = new File(fname+".txt");
        if (f.createNewFile()) {
            System.out.println("File created: " + f.getName());
        } else {
            System.out.println("File already exists.");
        }
        FileWriter fwriter = new FileWriter(f);
        fwriter.write(content);
        fwriter.close();
        Scanner myr = new Scanner(f);
        String data = "";
        while (myr.hasNextLine()){
            data = myr.nextLine();
        }
        myr.close();
        //initailize recbuf
        byte[] revbuf = new byte[5000];
        // get client data
        System.out.println(data);
        System.out.println("enter ip addr");
        String ipaddr = in.nextLine();
        System.out.println("enter port");
        int port = Integer.parseInt(in.nextLine());
        Message ms = new Message("START STORE",ipaddr,port,fname,content);
        ByteArrayOutputStream bout = new ByteArrayOutputStream(5000);
        ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(bout));
        os.writeObject(ms);
        os.flush();
        byte[] sendbuf = bout.toByteArray();
        DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(ipaddr),port);
        client.sock.send(pack);
        os.close();
        pack = new DatagramPacket(revbuf,revbuf.length);
        client.sock.receive(pack);
        Integer obtainedHash = Integer.parseInt(new String(pack.getData(),0,pack.getLength()));
        System.out.println(fname+" - hash value : "+obtainedHash);
    }

    public static void getFile(Integer hashcode) throws IOException {
        System.out.println("file save as ?");
        String fname = in.nextLine();
        String currentloc = System.getProperty("user.dir").replace("/","/");
        System.out.println("enter ip addr");
        String ipaddr = in.nextLine();
        System.out.println("enter port");
        int port = Integer.parseInt(in.nextLine());
        //recbuf init
        byte[] recbuf = new byte[1024];
        Message ms = new Message("GET",client.localIP,client.localPort,hashcode);
        ByteArrayOutputStream bout = new ByteArrayOutputStream(5000);
        ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(bout));
        os.writeObject(ms);
        os.flush();
        byte[] sendbuf = bout.toByteArray();
        DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(ipaddr),port);
        client.sock.send(pack);
        pack = new DatagramPacket(recbuf,recbuf.length);
        client.sock.receive(pack);
        String content = new String(pack.getData(),0,pack.getLength());
        System.out.println("obtained file content");
        System.out.println(content);
        File f = new File(currentloc+"/"+fname+".txt");
        FileWriter fw = new FileWriter(f);
        fw.write(content);
        fw.close();
    }
}
