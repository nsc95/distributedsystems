package edu.rit.cs;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class Tester {
    static LinkedHashMap<String,Integer> t = new LinkedHashMap<>();
    public static void main(String[] args) throws IOException {
        System.out.println(prefixMatch("1000","1000"));
        System.out.println(getBinString(0));
        String loc = System.getProperty("user.dir");
        loc = loc.replace("/", "/");
        File f = new File("somefile.txt");
        System.out.println(f.createNewFile());
        Scanner myreader = new Scanner(f);
        String data = "";
        while (myreader.hasNextLine()) {
            data = myreader.nextLine();
        }
        System.out.println(data);
        myreader.close();
    }

    public static void initRoutingTable()
    {
        String bin = Integer.toBinaryString(2);
        while (bin.length() < 4)
        {
            bin = "0"+bin;
        }
        StringBuilder str = new StringBuilder(bin);
        char ch = 'x';
        t.put(bin,2);
        for(int i=str.length()-1;i>=0;i--)
        {
            str.setCharAt(i,ch);
            t.put(str.toString(),-1);
        }
    }
    public static String prefixMatch(String bin1,String bin2)
    {
        String ans = "";
        int i = 0;
        while (i < 4)
        {
            if(bin1.charAt(i) == bin2.charAt(i))
            {
                ans = ans + bin1.charAt(i)+"";
            }
            else{
                break;
            }
            i++;
        }
        while (i < 4)
        {
            ans = ans + "x";
            i++;
        }
        return ans;
    }
    public static String getBinString(int x)
    {
        String bin = Integer.toBinaryString(x);
        while (bin.length() < 4)
        {
            bin = "0"+bin;
        }
        return bin;
    }
}
