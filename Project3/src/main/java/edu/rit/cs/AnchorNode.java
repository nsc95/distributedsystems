package edu.rit.cs;

import java.lang.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AnchorNode {

    static HashMap<Integer, String> ActiveAgents = new HashMap<>();
    static Set<Integer> anchors = new HashSet<Integer>();
    static LinkedHashMap<String, Integer> routingTable = new LinkedHashMap<>();
    static HashMap<Integer,String> FileMap = new HashMap<>();
    static HashMap<Integer,String> FileNameContent = new HashMap<>();
    static int port = -1;
    static int ID;
    static String localIP = null;
    static DatagramSocket recvSocket = null;
    static String otherAnchorIp = null;
    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            System.out.println("wrong number of cmd arguments");
            System.exit(-1);
        } else {
            try {
                AnchorNode.port = Integer.parseInt(args[0]);
                AnchorNode.ID = Integer.parseInt(args[1]);
                AnchorNode.otherAnchorIp = args[2];
            } catch (Exception e) {
                System.out.println("invalid cmd line arguments");
                System.exit(-1);
            }
        }
        new Showoptions().start();
        ExecutorService pool = Executors.newFixedThreadPool(4);
        if (!args[2].equals("none"))
        {
            InetAddress inetAddress = InetAddress.getLocalHost();
            AnchorNode.localIP = inetAddress.getHostAddress();
            ByteArrayOutputStream bao = new ByteArrayOutputStream(5000);
            ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(bao));
            os.flush();os.writeObject(new Message(AnchorNode.ID,"ANCHOR",AnchorNode.localIP,AnchorNode.port,AnchorNode.ID));
            os.flush();byte[] sendbuf = bao.toByteArray();
            AnchorNode.recvSocket = new DatagramSocket(AnchorNode.port);
            AnchorNode.recvSocket.send(new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(args[2]),9005));
            os.close();
        }
        else{
            AnchorNode.recvSocket = new DatagramSocket(AnchorNode.port);
            InetAddress inetAddress = InetAddress.getLocalHost();
            AnchorNode.localIP = inetAddress.getHostAddress();
        }
        try {
            AnchorNode.anchors.add(AnchorNode.ID);
            initRoutingTable();
            System.out.println("server running at " + AnchorNode.localIP + " : " + AnchorNode.port);
            while (true) {
                System.out.println("waiting for packets");
                byte[] recvBuf = new byte[5000];
                DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
                AnchorNode.recvSocket.receive(packet);
                System.out.println(packet.getPort());
                System.out.println(packet.getAddress());
                int bytecount = packet.getLength();
                pool.execute(new MessageHandler(bytecount, packet, recvBuf));
            }
        } catch (IOException e) {
            System.out.println("IOException in main....");
            e.printStackTrace();
            AnchorNode.recvSocket.close();
        }
    }

    public static void initRoutingTable()
    {
        String bin = Integer.toBinaryString(AnchorNode.ID);
        while (bin.length() < 4)
        {
            bin = "0"+bin;
        }
        StringBuilder str = new StringBuilder(bin);
        char ch = 'x';
        AnchorNode.routingTable.put(bin,AnchorNode.ID);
        for(int i=str.length()-1;i>=0;i--)
        {
            str.setCharAt(i,ch);
            AnchorNode.routingTable.put(str.toString(),-1);
        }
    }
}

class MessageHandler implements Runnable{

    int bytecount = -1;
    DatagramPacket packet = null;
    byte[] recvBuf = null;
    ByteArrayInputStream bstream = null;
    ObjectInputStream istream = null;
    Message recvobj = null;
    ByteArrayOutputStream boutstream = null;
    ObjectOutputStream os = null;
    public MessageHandler(int bytecount,DatagramPacket packet,byte[] recvBuf){
        this.bytecount = bytecount;
        this.packet = packet;
        this.recvBuf = recvBuf;
        this.bstream = new ByteArrayInputStream(this.recvBuf);
        try {
            this.istream = new ObjectInputStream(new BufferedInputStream(bstream));
            this.recvobj = (Message)istream.readObject();
            this.istream.close();
        } catch (IOException e) {
            System.out.println("exception in packet recieve");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found exception");
            e.printStackTrace();
        }
    }
    public void run(){
        if(this.recvobj.getType().equals("ANCHOR"))
        {
            try {
                this.handleAnchor();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("JOIN"))
        {
            try {
                System.out.println("join request came");
                this.handleJoin();
            } catch (IOException e) {
                System.out.println("exception IO in run() handlejoin");
                e.printStackTrace();
            }
        }
        else if (this.recvobj.getType().equals("LOOK UP"))
        {
            try {
                this.lookup(this.recvobj.getId());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("START STORE"))
        {
            try {
                this.handleStartStore();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("STORE"))
        {
            try {
                this.handleStore();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("GET"))
        {
            try {
                this.handleGet();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("EXIT STORE"))
        {
            try {
                this.handleExitStore();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void handleExitStore() throws IOException {
        Integer hashcode = this.recvobj.getHashcode();
        Integer nodeid = this.recvobj.getId();
        this.removeFromRoutingTable(nodeid);
        String filename = this.recvobj.getFilename();
        String content = this.recvobj.getContent();
        System.out.println("node "+nodeid+" exited....storing here");
        String currentloc = System.getProperty("user.dir").replace("/","/");
        System.out.println("node "+nodeid+ " exited....storing here");
        File file = new File(currentloc+"/"+filename);
        if (file.createNewFile()) {
            System.out.println("File created: " + filename);
        } else {
            System.out.println("File already exists.");
        }
        FileWriter fwriter = new FileWriter(file);
        fwriter.write(content);
        fwriter.close();
        synchronized (AnchorNode.FileMap)
        {
            AnchorNode.FileMap.put(hashcode,filename);
            AnchorNode.FileNameContent.put(hashcode,content);
        }
    }

    public void removeFromRoutingTable(Integer nodeid){
        synchronized (AnchorNode.routingTable)
        {
            System.out.println("came to remove "+nodeid);
            for(Map.Entry mapele : AnchorNode.routingTable.entrySet())
            {
                Integer value = (Integer)mapele.getValue();
                if(value.equals(nodeid)) {
                    System.out.println("trying to change value");
                    mapele.setValue(-1);
                }
            }
        }
    }

    public void handleGet() throws IOException {
        System.out.println("came to get");
        int hashcode = this.recvobj.getHashcode();
        if(AnchorNode.FileMap.containsKey(hashcode))
        {
            String filename = AnchorNode.FileMap.get(hashcode);
            String currentloc = System.getProperty("user.dir");
            String content = AnchorNode.FileNameContent.get(hashcode);
            System.out.println(content);
            byte[] sendbuf = content.getBytes();
            AnchorNode.recvSocket.send(new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(this.recvobj.getIP()),this.recvobj.getPort()));
            System.out.println("Sent file to client");
        }
        else{
            String match = this.prefixMatch(this.getBinString(AnchorNode.ID),this.getBinString(hashcode));
            int tosendId = AnchorNode.routingTable.get(match);
            if(tosendId == -1)
            {
                System.out.println("missing getFile stopped here");
            }
            else{
                System.out.println("sending get request");
                this.boutstream = new ByteArrayOutputStream(5000);
                os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
                os.flush();
                os.writeObject(new Message(this.recvobj.getType(),this.recvobj.getIP(),this.recvobj.getPort(),this.recvobj.getHashcode()));
                os.flush();
                byte[] sendbuf = this.boutstream.toByteArray();
                String[] tosendinfo =  AnchorNode.ActiveAgents.get(tosendId).split(",");
                DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(tosendinfo[0]),
                        Integer.parseInt(tosendinfo[1]));
                AnchorNode.recvSocket.send(pack);
            }
        }
    }

    public void handleStartStore() throws IOException {
        System.out.println(" came to store ");
        //File file = this.recvobj.getFile();
        String fname = this.recvobj.getFilename();
        String content = this.recvobj.getContent();
        System.out.println(" file name : "+fname);
        System.out.println("content "+content);
        int hashval = content.hashCode()%16;
        hashval = hashval < 0 ? hashval*(-1) : hashval;
        byte[] hashclient = Integer.toString(hashval).getBytes();
        DatagramPacket sendtoclient = new DatagramPacket(hashclient,hashclient.length,this.packet.getAddress(),this.packet.getPort());
        AnchorNode.recvSocket.send(sendtoclient);
        System.out.println("hashvalue "+hashval);
        if(AnchorNode.ID == hashval)
        {
            System.out.println("FOUND! Storing here");
            String currentloc = System.getProperty("user.dir");
            currentloc = currentloc.replace("/","/");
            File file = new File(currentloc+"/"+fname);
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter fwriter = new FileWriter(file);
            fwriter.write(content);
            fwriter.close();
            AnchorNode.FileMap.put(hashval,fname);
            AnchorNode.FileNameContent.put(hashval,content);
            return;
        }
        String match = this.prefixMatch(this.getBinString(AnchorNode.ID),this.getBinString(hashval));
        int tosendId = AnchorNode.routingTable.get(match);
        if(tosendId == -1)
        {
            System.out.println("value missing");
            System.out.println("storing at "+AnchorNode.ID);
            String currentloc = System.getProperty("user.dir");
            currentloc = currentloc.replace("/","/");
            File file = new File(currentloc+"/"+fname);
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            } else {
                System.out.println("File already exists.");
            }
            FileWriter fwriter = new FileWriter(file);
            fwriter.write(content);
            fwriter.close();
            AnchorNode.FileMap.put(hashval,fname);
            AnchorNode.FileNameContent.put(hashval,content);
        }
        else{
            System.out.println("sending store....");
            this.boutstream = new ByteArrayOutputStream(5000);
            os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
            os.flush();
            os.writeObject(new Message(hashval,"STORE",AnchorNode.localIP,AnchorNode.port,AnchorNode.ID,fname,
                    content));
            os.flush();
            byte[] sendbuf = this.boutstream.toByteArray();
            String[] tosendinfo = AnchorNode.ActiveAgents.get(tosendId).split(",");
            DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(tosendinfo[0]),Integer.parseInt(tosendinfo[1]));
            AnchorNode.recvSocket.send(pack);
        }
    }

    public void handleStore() throws IOException {
        int hashvalue = this.recvobj.getId();
        String filename = this.recvobj.getFilename();
        String content = this.recvobj.getContent();
        if(AnchorNode.ID == hashvalue)
        {
            System.out.println("FOUND here storing");
            String currentloc = System.getProperty("user.dir");
            currentloc = currentloc.replace("/","/");
            File file = new File(currentloc+"/"+filename);
            if (file.createNewFile()) {
                System.out.println("File created: " + filename);
            } else {
                System.out.println("File already exists.");
            }
            FileWriter fwriter = new FileWriter(file);
            fwriter.write(content);
            fwriter.close();
            AnchorNode.FileMap.put(hashvalue,file.getName());
            AnchorNode.FileNameContent.put(hashvalue,content);
            return;
        }
        String match = this.prefixMatch(this.getBinString(AnchorNode.ID),this.getBinString(hashvalue));
        int tosendId = AnchorNode.routingTable.get(match);
        if(tosendId == -1)
        {
            System.out.println("value missing");
            System.out.println("storing at "+AnchorNode.ID);
            String currentloc = System.getProperty("user.dir");
            currentloc = currentloc.replace("/","/");
            File file = new File(currentloc+"/"+filename);
            if (file.createNewFile()) {
                System.out.println("File created: " + filename);
            } else {
                System.out.println("File already exists.");
            }
            FileWriter fwriter = new FileWriter(file);
            fwriter.write(content);
            fwriter.close();
            AnchorNode.FileMap.put(hashvalue,filename);
            AnchorNode.FileNameContent.put(hashvalue,content);
        }
        else{
            System.out.println("sending store....");
            this.boutstream = new ByteArrayOutputStream(5000);
            os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
            os.flush();
            os.writeObject(new Message(hashvalue,"STORE",AnchorNode.localIP,AnchorNode.port,AnchorNode.ID,filename,
                    content));
            os.flush();
            byte[] sendbuf = this.boutstream.toByteArray();
            String[] tosendinfo = AnchorNode.ActiveAgents.get(tosendId).split(",");
            DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(tosendinfo[0]),Integer.parseInt(tosendinfo[1]));
            AnchorNode.recvSocket.send(pack);
        }
    }

    public void handleAnchor() throws IOException {
        synchronized (AnchorNode.anchors)
        {
            AnchorNode.anchors.add(this.recvobj.getId());
        }
        synchronized (AnchorNode.ActiveAgents)
        {
            AnchorNode.ActiveAgents.put(this.recvobj.getId(),this.recvobj.getIP()+","+this.recvobj.getPort());
        }
        this.lookup(this.recvobj.getId());
    }

    public void lookup(int id) throws IOException {
        this.updateRoutingTable(this.getBinString(id),this.recvobj.getIP(),this.recvobj.getPort());
        this.updateRoutingTable(this.getBinString(this.recvobj.getFrom()),this.packet.getAddress().toString().replaceFirst("/","")
                ,this.packet.getPort());
        if(id == AnchorNode.ID) {
            System.out.println("FOUND here");
            return;
        }
        String match = this.prefixMatch(this.getBinString(AnchorNode.ID),this.getBinString(id));
        int tosendId = AnchorNode.routingTable.get(match);
        if(tosendId == -1)
        {
            System.out.println("value missing...");
            System.out.println("lookup stopped here");
        }
        else{
            System.out.println("sending...lookup packet");
            this.boutstream = new ByteArrayOutputStream(5000);
            os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
            os.writeObject(new Message(this.recvobj.getId(),"LOOK UP",this.recvobj.getIP(),this.recvobj.getPort(),AnchorNode.ID));
            os.flush();
            byte[] sendbuf = this.boutstream.toByteArray();
            String[] tosendinfo = AnchorNode.ActiveAgents.get(tosendId).split(",");
            DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(tosendinfo[0]),Integer.parseInt(tosendinfo[1]));
            AnchorNode.recvSocket.send(pack);
        }
    }

    public void handleJoin() throws IOException {
        if((AnchorNode.ID == 2) && (AnchorNode.ID^this.recvobj.getId()) > (4^this.recvobj.getId()) ||
                (AnchorNode.ID == 4) && (AnchorNode.ID^this.recvobj.getId()) > (2^this.recvobj.getId()))
        {
            String[] destinfo = null;
            if(AnchorNode.ID == 2)
                destinfo = AnchorNode.ActiveAgents.get(4).split(",");
            else
                destinfo = AnchorNode.ActiveAgents.get(2).split(",");
            this.boutstream = new ByteArrayOutputStream(5000);
            this.os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
            this.os.writeObject(new Message(this.recvobj.getId(), "JOIN", this.recvobj.getIP(), this.recvobj.getPort(),AnchorNode.ID));
            this.os.flush();
            byte[] sendbuf = boutstream.toByteArray();
            this.packet = new DatagramPacket(sendbuf, sendbuf.length, InetAddress.getByName(destinfo[0]), Integer.parseInt(destinfo[1]));
            AnchorNode.recvSocket.send(this.packet);
        }
        else {
            this.boutstream = new ByteArrayOutputStream(5000);
            this.os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
            this.os.writeObject(new Message(this.recvobj.getId(), "JOINED", this.recvobj.getIP(), this.recvobj.getPort(),AnchorNode.ID));
            this.os.flush();
            byte[] sendbuf = boutstream.toByteArray();
            this.packet = new DatagramPacket(sendbuf, sendbuf.length, InetAddress.getByName(this.recvobj.getIP()), this.recvobj.getPort());
            AnchorNode.recvSocket.send(this.packet);
            String ipaddr = this.recvobj.getIP();
            int port = this.recvobj.getPort();
            int id = this.recvobj.getId();
            synchronized (AnchorNode.ActiveAgents) {
                if (!AnchorNode.ActiveAgents.containsKey(id)) {
                    AnchorNode.ActiveAgents.put(id, ipaddr + "," + port);
                }
            }
            this.lookup(this.recvobj.getId());
            String[] destinfo = null;
            //sending lookup to other anchor node
            if(AnchorNode.ID == 2)
                destinfo = AnchorNode.ActiveAgents.get(4).split(",");
            else
                destinfo = AnchorNode.ActiveAgents.get(2).split(",");
            this.os.writeObject(new Message(this.recvobj.getId(), "LOOK UP", this.recvobj.getIP(), this.recvobj.getPort(),AnchorNode.ID));
            this.os.flush();
            sendbuf = boutstream.toByteArray();
            this.packet = new DatagramPacket(sendbuf, sendbuf.length, InetAddress.getByName(destinfo[0]), Integer.parseInt(destinfo[1]));
            System.out.println("Sending lookup after joined to ");
            AnchorNode.recvSocket.send(this.packet);
        }
    }

    public void updateRoutingTable(String binstr,String IP,int port)
    {
        String curbinstr = this.getBinString(AnchorNode.ID);
        String prefixmatch = this.prefixMatch(curbinstr,binstr);
        synchronized (AnchorNode.routingTable) {
            if (AnchorNode.routingTable.get(prefixmatch) == -1) {
                AnchorNode.routingTable.put(prefixmatch,Integer.parseInt(binstr,2));
            }
            else{
                int existingval = AnchorNode.routingTable.get(prefixmatch);
                int curval = Integer.parseInt(binstr,2);
                if((AnchorNode.ID^curval) < (AnchorNode.ID^existingval))
                    AnchorNode.routingTable.put(prefixmatch,curval);
            }
        }
        synchronized (AnchorNode.ActiveAgents)
        {
            if(!AnchorNode.ActiveAgents.containsKey(Integer.parseInt(binstr,2)))
            {
                AnchorNode.ActiveAgents.put(Integer.parseInt(binstr,2),IP+","+port);
            }
        }
        System.out.println("routing table updated");
    }

    public String prefixMatch(String bin1,String bin2)
    {
        String ans = "";
        int i = 0;
        while (i < 4)
        {
            if(bin1.charAt(i) == bin2.charAt(i))
            {
                ans = ans + bin1.charAt(i)+"";
            }
            else{
                break;
            }
            i++;
        }
        while (i < 4)
        {
            ans = ans + "x";
            i++;
        }
        return ans;
    }

    public String getBinString(int x)
    {
        String bin = Integer.toBinaryString(x);
        while (bin.length() < 4)
        {
            bin = "0"+bin;
        }
        return bin;
    }

}

class Showoptions extends Thread {
    public void run() {
        Scanner in = new Scanner(System.in);
        while (true) {
            String option = in.nextLine();
            switch (option) {
                case "1":
                    System.out.println(AnchorNode.routingTable);
                    break;
                case "2":
                    System.out.println(AnchorNode.ActiveAgents);
                    break;
                case "4":
                    System.out.println(AnchorNode.FileNameContent);
                    break;
                default:
                    System.out.println("wrong option");
                    break;
            }
        }
    }
}
