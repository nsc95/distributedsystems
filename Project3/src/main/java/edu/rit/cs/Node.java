package edu.rit.cs;

import javax.xml.crypto.Data;
import java.io.*;
import java.net.*;
import java.util.*;

public class Node {
    static int ID = -1;
    static LinkedHashMap<String,Integer> routingTable = new LinkedHashMap<>();
    static String localIP = null;
    static int port = -1;
    static InetAddress servAddress = null;
    static int servPort = -1;
    static DatagramSocket sendSock = null;
    static Integer anchorID = -1;
    static String anchorAddress = "";
    static Integer anchorPort = -1;
    static HashMap<Integer,String> FileMap = new HashMap<>();
    static HashMap<Integer,String> FileNameContent = new HashMap<>();
    static HashMap<Integer,String> ActiveAgents = new HashMap<>();
    static boolean safeexit = false;
    public static void main(String[] args) throws IOException {
        if(args.length != 4)
        {
            System.out.println("wrong number of cmd args");
            System.exit(-1);
        }
        else{

            try {
                InetAddress inetAddress = InetAddress.getLocalHost();
                Node.localIP = inetAddress.getHostAddress();
                Node.ID = Integer.parseInt(args[0]);
                Node.servAddress = InetAddress.getByName(args[1]);
                Node.servPort = Integer.parseInt(args[2]);
                Node.port = Integer.parseInt(args[3]);
                Node.sendSock = new DatagramSocket(Node.port);
                Message ms = new Message(Node.ID,"JOIN",Node.localIP,Node.port,Node.ID);
                if(!sendToAnchorNode(ms))
                {
                    System.out.println("could not join network");
                }
                System.out.println("joined network");
                initRoutingTable();
                new ShowoptionsNode().start();
                while (true)
                {
                    byte[] recvBuf = new byte[5000];
                    DatagramPacket packet = new DatagramPacket(recvBuf,recvBuf.length);
                    Node.sendSock.receive(packet);
                    int bytecount = packet.getLength();
                    new Thread(new MessageHandlerNode(bytecount,packet,recvBuf)).start();
                }
            } catch (UnknownHostException e) {
                System.out.println("Exception in trying to know host");
                e.printStackTrace();
                System.exit(-1);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                System.exit(-1);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public static boolean sendToAnchorNode(Message ms) throws IOException, ClassNotFoundException {
        ObjectOutputStream os = null;
        ByteArrayOutputStream boutstream = new ByteArrayOutputStream(5000);
        os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
        os.flush();
        os.writeObject(ms);
        os.flush();
        byte[] sendBuf = boutstream.toByteArray();
        DatagramPacket packet = new DatagramPacket(sendBuf,sendBuf.length,Node.servAddress,Node.servPort);
        Node.sendSock.send(packet);
        System.out.println("sent to "+Node.servAddress+" "+Node.servPort);
        ObjectInputStream oi = null;
            try {
                byte[] rbuf = new byte[5000];
                DatagramPacket pack = new DatagramPacket(rbuf,rbuf.length);
                Node.sendSock.receive(pack);
                ByteArrayInputStream bstream = new ByteArrayInputStream(rbuf);
                oi = new ObjectInputStream(new BufferedInputStream(bstream));
                Message m = (Message) oi.readObject();
                if (m.getType().equals("JOINED")) {
                    System.out.println("joined with anchor "+m.getId());
                    Node.anchorAddress = pack.getAddress().toString();
                    Node.anchorID = m.getFrom();
                    Node.anchorPort = pack.getPort();
                    return true;
                }
            }catch (Exception e)
            {
                System.out.println("exception could not join");
                os.close();
                oi.close();
                return false;
            }
        os.close();
        oi.close();
        return true;
    }

    public static void initRoutingTable()
    {
        String bin = Integer.toBinaryString(Node.ID);
        while (bin.length() < 4)
        {
            bin = "0"+bin;
        }
        StringBuilder str = new StringBuilder(bin);
        char ch = 'x';
        Node.routingTable.put(bin,Node.ID);
        for(int i=str.length()-1;i>=0;i--)
        {
            str.setCharAt(i,ch);
            Node.routingTable.put(str.toString(),-1);
        }
        System.out.println(Node.routingTable);
    }
}

class MessageHandlerNode implements Runnable{

    int bytecount = -1;
    DatagramPacket packet = null;
    byte[] recvBuf = null;
    ByteArrayInputStream bstream = null;
    ObjectInputStream istream = null;
    ByteArrayOutputStream boutstream = null;
    ObjectOutputStream os = null;
    Message recvobj = null;
    public MessageHandlerNode(int bytecount,DatagramPacket packet,byte[] recvBuf) throws InterruptedException {
        this.bytecount = bytecount;
        this.packet = packet;
        this.recvBuf = recvBuf;
        Thread.sleep(100);
        this.bstream = new ByteArrayInputStream(this.recvBuf);
        try {
            this.istream = new ObjectInputStream(new BufferedInputStream(bstream));
            this.recvobj = (Message)istream.readObject();
            this.istream.close();
        } catch (IOException e) {
            System.out.println("exception in packet recieve");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found exception");
            e.printStackTrace();
        }
    }

    public MessageHandlerNode(Message msg)
    {
        this.recvobj = msg;
    }

    public void run(){
        if(this.recvobj.getType().equals("LOOK UP"))
        {
            try {
                this.lookup(this.recvobj.getId());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("STORE"))
        {
            try {
                this.handleStore();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("GET"))
        {
            try{
                this.handleGet();
            }catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("EXIT"))
        {
            try {
                this.handleExit();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(this.recvobj.getType().equals("EXIT STORE"))
        {
            try {
                this.handleExitStore();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void handleExit() throws IOException {
        String currentloc = System.getProperty("user.dir").replace("/","/");
        List<Integer> nodeIdList = this.getNodesFromTable();
        for(Map.Entry mapElement : Node.FileMap.entrySet())
        {
            String filename = (String)mapElement.getValue();
            String content = (String)(Node.FileNameContent.get((Integer)mapElement.getKey()));
            Message ms = new Message(Node.ID,"EXIT STORE",Node.localIP,Node.port,content,filename,
                    (Integer)mapElement.getKey());
            this.boutstream = new ByteArrayOutputStream(5000);
            os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
            os.flush();os.writeObject(ms);os.flush();
            byte[] sendbuf = this.boutstream.toByteArray();
            for(Integer id: nodeIdList)
            {
                String[] tosendinfo = Node.ActiveAgents.get(id).split(",");
                DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(tosendinfo[0].replace("/","")),
                        Integer.parseInt(tosendinfo[1]));
                Node.sendSock.send(pack);
            }
            os.close();
        }
        Node.safeexit = true;
    }

    public void handleExitStore() throws IOException {
        String ip = this.recvobj.getIP();
        Integer port = this.recvobj.getPort();
        Integer nodeid = this.recvobj.getId();
        Integer hashcode = this.recvobj.getHashcode();
        this.removeFromRoutingTable(nodeid);
        String filename = this.recvobj.getFilename();
        String content = this.recvobj.getContent();
        System.out.println("node "+nodeid+ " exited....storing here");
        String currentloc = System.getProperty("user.dir").replace("/","/");
        File file = new File(currentloc+"/"+filename);
        if (file.createNewFile()) {
            System.out.println("File created: " + filename);
        } else {
            System.out.println("File already exists.");
        }
        FileWriter fwriter = new FileWriter(file);
        fwriter.write(content);
        fwriter.close();
        synchronized (Node.FileMap)
        {
            Node.FileMap.put(hashcode,filename);
            Node.FileNameContent.put(hashcode,content);
        }
    }

    public void removeFromRoutingTable(Integer nodeid)
    {
        synchronized (Node.routingTable)
        {
            for(Map.Entry mapele : Node.routingTable.entrySet())
            {
                Integer value = (Integer)mapele.getValue();
                if(value.equals(nodeid)) {
                    System.out.println("trying to change value");
                    mapele.setValue(-1);
                }
            }
        }
    }

    public List<Integer> getNodesFromTable(){
        List<Integer> nodeIdlist = new ArrayList<>();
        for(Map.Entry mapElement : Node.routingTable.entrySet())
        {
            Integer nodeid = (Integer)mapElement.getValue();
            if(nodeid != -1 && nodeid != Node.ID)
                nodeIdlist.add(nodeid);
        }
        return nodeIdlist;
    }
    public void handleGet() throws IOException {
        System.out.println("came to get");
        int hashcode = this.recvobj.getHashcode();
        if(Node.FileMap.containsKey(hashcode))
        {
            String filename = Node.FileMap.get(hashcode);
            String currentloc = System.getProperty("user.dir").replace("/","/");
            String content = Node.FileNameContent.get(hashcode);
            byte[] sendbuf = content.getBytes();
            Node.sendSock.send(new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(this.recvobj.getIP()),
                    this.recvobj.getPort()));
            System.out.println("sent file to client");
        }
        else{
            String match = this.prefixMatch(this.getBinString(Node.ID),this.getBinString(hashcode));
            int tosendId = Node.routingTable.get(match);
            if(tosendId == -1)
            {
                System.out.println("missing getFile stopped here");
            }
            else{
                System.out.println("sending get request");
                this.boutstream = new ByteArrayOutputStream(5000);
                os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
                os.flush();
                os.writeObject(new Message(this.recvobj.getType(),this.recvobj.getIP(),this.recvobj.getPort(),this.recvobj.getHashcode()));
                os.flush();
                byte[] sendbuf = this.boutstream.toByteArray();
                String[] tosendinfo =  Node.ActiveAgents.get(tosendId).split(",");
                DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(tosendinfo[0].replace("/","")),
                        Integer.parseInt(tosendinfo[1]));
                Node.sendSock.send(pack);
            }
        }
    }

    public void handleStore() throws IOException {
        int hashvalue = this.recvobj.getId();
        String filename = this.recvobj.getFilename();
        String content = this.recvobj.getContent();
        if(Node.ID == hashvalue)
        {
            System.out.println("FOUND here storing");
            String currentloc = System.getProperty("user.dir");
            currentloc = currentloc.replace("/","/");
            File file = new File(currentloc+"/"+filename);
            if (file.createNewFile()) {
                System.out.println("File created: " + filename);
            } else {
                System.out.println("File already exists.");
            }
            FileWriter fwriter = new FileWriter(file);
            fwriter.write(content);
            fwriter.close();
            Node.FileMap.put(hashvalue,filename);
            Node.FileNameContent.put(hashvalue,content);
            return;
        }
        String match = this.prefixMatch(this.getBinString(Node.ID),this.getBinString(hashvalue));
        int tosendId = Node.routingTable.get(match);
        if(tosendId == -1)
        {
            System.out.println("value missing");
            System.out.println("storing at "+Node.ID);
            String currentloc = System.getProperty("user.dir");
            currentloc = currentloc.replace("/","/");
            File file = new File(currentloc+"/"+filename);
            if (file.createNewFile()) {
                System.out.println("File created: " + filename);
            } else {
                System.out.println("File already exists.");
            }
            FileWriter fwriter = new FileWriter(file);
            fwriter.write(content);
            fwriter.close();
            Node.FileMap.put(hashvalue,filename);
            Node.FileNameContent.put(hashvalue,content);
        }
        else{
            System.out.println("sending store....");
            this.boutstream = new ByteArrayOutputStream(5000);
            os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
            os.flush();
            os.writeObject(new Message(hashvalue,"STORE",Node.localIP,Node.port,Node.ID,filename,content));
            os.flush();
            byte[] sendbuf = this.boutstream.toByteArray();
            String[] tosendinfo = Node.ActiveAgents.get(tosendId).split(",");
            DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(tosendinfo[0].replace("/","")),Integer.parseInt(tosendinfo[1]));
            Node.sendSock.send(pack);
        }
    }
    public void updateRoutingTable(String binstr,String IP,int port)
    {
        String curbinstr = this.getBinString(Node.ID);
        String prefixmatch = this.prefixMatch(curbinstr,binstr);
        synchronized (Node.routingTable) {
            if (Node.routingTable.get(prefixmatch) == -1) {
                Node.routingTable.put(prefixmatch,Integer.parseInt(binstr,2));
            }
            else{
                int existingval = Node.routingTable.get(prefixmatch);
                int curval = Integer.parseInt(binstr,2);
                if((Node.ID^curval) < (Node.ID^existingval))
                    Node.routingTable.put(prefixmatch,curval);
            }
        }
        synchronized (Node.ActiveAgents)
        {
            if(!Node.ActiveAgents.containsKey(Integer.parseInt(binstr,2)))
            {
                Node.ActiveAgents.put(Integer.parseInt(binstr,2),IP+","+port);
            }
        }
        System.out.println("routing table updated");
    }

    public void lookup(int id) throws IOException {
        this.updateRoutingTable(this.getBinString(id),this.recvobj.getIP(),this.recvobj.getPort());
        this.updateRoutingTable(this.getBinString(this.recvobj.getFrom()),this.packet.getAddress().toString(),this.packet.getPort());
        if(id == Node.ID) {
            System.out.println("FOUND here");
            return;
        }
        String match = this.prefixMatch(this.getBinString(Node.ID),this.getBinString(id));
        int tosendId = Node.routingTable.get(match);
        if(tosendId == -1)
        {
            System.out.println("value missing...");
            System.out.println("lookup stopped here");
        }
        else{
            System.out.println("sending...lookup packet");
            this.boutstream = new ByteArrayOutputStream(5000);
            os = new ObjectOutputStream(new BufferedOutputStream(boutstream));
            os.flush();
            os.writeObject(new Message(this.recvobj.getId(),"LOOK UP",this.recvobj.getIP(),this.recvobj.getPort(),Node.ID));
            os.flush();
            byte[] sendbuf = this.boutstream.toByteArray();
            String[] tosendinfo = Node.ActiveAgents.get(tosendId).split(",");
            DatagramPacket pack = new DatagramPacket(sendbuf,sendbuf.length,InetAddress.getByName(tosendinfo[0]),Integer.parseInt(tosendinfo[1]));
            Node.sendSock.send(pack);
        }
    }

    public String getBinString(int x)
    {
        String bin = Integer.toBinaryString(x);
        while (bin.length() < 4)
        {
            bin = "0"+bin;
        }
        return bin;
    }
    public String prefixMatch(String bin1,String bin2)
    {
        String ans = "";
        int i = 0;
        while (i < 4)
        {
            if(bin1.charAt(i) == bin2.charAt(i))
            {
                ans = ans + bin1.charAt(i)+"";
            }
            else{
                break;
            }
            i++;
        }
        while (i < 4)
        {
            ans = ans + "x";
            i++;
        }
        return ans;
    }
}

class ShowoptionsNode extends Thread{
    public void run(){
        Scanner in = new Scanner(System.in);
        while (true) {
            boolean loop = true;
            String option = in.nextLine();
            switch (option)
            {
                case "1":
                    System.out.println(Node.routingTable);
                    break;
                case "2":
                    System.out.println(Node.ActiveAgents);
                    break;
                case "4":
                    System.out.println(Node.FileNameContent);
                    break;
                case "3" :
                    System.out.println("exiting...");
                    Message msg = new Message("EXIT");
                    new Thread(new MessageHandlerNode(msg)).start();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    System.out.println("wrong option");
                    break;
            }
            if(Node.safeexit)
                break;
        }
        System.exit(-1);
    }
}

