package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

    public static void main(String[] args)
    {
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        DataOutputStream dos = null;
        DataInputStream dis = null;
        File  inputcsv = new File("Project1/amazon-fine-food-reviews/Reviews.csv");
        String serveraddr = args[0];
        int serverPort = Integer.parseInt("7896");
        Socket s = null;
        try {
            s = new Socket(serveraddr, serverPort);
            byte[] filebytear = new byte[1024];
            fis = new FileInputStream(inputcsv);
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            int read;

            long file_size = inputcsv.length();
            dos = new DataOutputStream(s.getOutputStream());
            dos.writeLong(file_size);

            while((read = dis.read(filebytear))!= -1) {
                dos.write(filebytear, 0, read);
                dos.flush();
            }
            System.out.println("sent");
            int buffsize = s.getReceiveBufferSize();
            dis = new DataInputStream(s.getInputStream());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File("output.txt")));
            byte[] buffer = new byte[buffsize];
            while((read = dis.read(buffer)) != -1)
            {
                bos.write(buffer,0,read);
                bos.flush();
            }
            System.out.println("got from server");
            BufferedReader reader = new BufferedReader(new FileReader(new File("output.txt")));
            String line = reader.readLine();
            while(line != null)
            {
                System.out.println(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (UnknownHostException e) {
            System.out.println("Sock:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO:" + e.getMessage());
        } finally {
            if (s != null)
                try {
                    s.close();
                } catch (IOException e) {
                    System.out.println("close:" + e.getMessage());
                }
        }
    }
}
