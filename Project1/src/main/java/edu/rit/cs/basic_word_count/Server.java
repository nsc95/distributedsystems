package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.Map;

public class Server {

    public static void main(String args[]) {
        try {
            int serverPort = 7896;
            InetAddress iaddre = InetAddress.getLocalHost();
            System.out.println(iaddre.getHostAddress());
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("TCP Server is running and accepting client connections...");
            while (true) {
                Socket clientSocket = listenSocket.accept();
                System.out.println("accepted connection");
                Connection c = new Connection(clientSocket);
            }

        } catch (IOException e) {
            System.out.println("Listen :" + e.getMessage());
        }
    }
}

class Connection extends Thread {
    FileOutputStream fos = null;
    BufferedOutputStream bos = null;
    OutputStream out = null;
    Socket clientSocket;
    DataInputStream clientdata =  null;
    InputStream in;
    DataOutputStream dos = null;
    public Connection(Socket aClientSocket) {
        this.clientSocket = aClientSocket;
        this.start();
    }

    public void run() {
        try {
            System.out.println("server connected with client");
            int buffsize = this.clientSocket.getReceiveBufferSize();
            this.in = this.clientSocket.getInputStream();
            this.clientdata = new DataInputStream(in);
            out = new FileOutputStream("test.csv");
            byte[] buffer = new byte[buffsize];
            int read;
            long total_file_size = this.clientdata.readLong();

            System.out.println("Total file size to receive" + total_file_size);
            int received_bytes = 0;

            while(received_bytes < total_file_size && (read = this.clientdata.read(buffer)) != -1)
            {
                out.write(buffer,0,read);
                received_bytes += read;
                out.flush();
            }
            System.out.println("recived file...now processing...");
            List<AmazonFineFoodReview> allrevs = WordCount_Seq_Improved.read_reviews("test.csv");
            System.out.println("Finished reading all reviews, now performing word count...");
            MyTimer myMapTimer = new MyTimer("map operation");
            myMapTimer.start_timer();
            List<KV<String, Integer>> kv_pairs = WordCount_Seq_Improved.map(allrevs);
            myMapTimer.stop_timer();

            MyTimer myReduceTimer = new MyTimer("reduce operation");
            myReduceTimer.start_timer();
            Map<String, Integer> results = WordCount_Seq_Improved.reduce(kv_pairs);
            myReduceTimer.stop_timer();

            File outputfile = new File("output.txt");
            FileWriter writer = new FileWriter(outputfile);
            for(Map.Entry<String, Integer> entry : results.entrySet())
            {
                writer.write(entry.getKey()+" : "+entry.getValue()+"\n");
            }
            writer.flush();
            writer.close();
            myMapTimer.print_elapsed_time();
            myReduceTimer.print_elapsed_time();
            myReduceTimer.print_elapsed_time();

            FileInputStream fis = new FileInputStream(new File("output.txt"));
            BufferedInputStream bis = new BufferedInputStream(fis);
            byte[] sendar = new byte[1024];
            DataInputStream filesendstream = new DataInputStream(bis);
            dos = new DataOutputStream(clientSocket.getOutputStream());
            System.out.println("sending....");
            while((read = filesendstream.read(sendar))!= -1)
            {
                dos.write(sendar,0,read);
                dos.flush();
            }

           System.out.println("sent");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                clientSocket.close();

            } catch (IOException e) {/*close failed*/}
        }
    }
}

