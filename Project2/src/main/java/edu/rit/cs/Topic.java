package edu.rit.cs;

public class Topic {
    Integer id;
    String name;
    public Topic(Integer id,String name)
    {
        this.id = id;
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
