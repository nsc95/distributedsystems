package edu.rit.cs;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EventManager extends Thread{
    static int servport = 9000;
    static Set<Topic> topicsSet = new HashSet<>();
    static HashMap<Integer,Topic> TopicMap = new HashMap<>();
    static HashMap<String, Boolean> ActiveAgents = new HashMap<>();
    static HashMap<String,String> AgentIpPort = new HashMap<>();
    static HashMap<String,Set<Topic>> SubMap = new HashMap<>();
    static HashMap<Integer,List<String>> TopicAgentMap = new HashMap<>();
    static HashMap<Event,List<String>> NotificationMap = new HashMap<>();
    static Integer GlobalEventIDGen = 1;
    static Integer GlobalTopicIDGen = 3;
    static ServerSocket servsock;
    int choice;
    Event advertisement;
    public EventManager(int choice){
        this.choice = choice;
    }
    public EventManager(int choice,Event ad){
        this.advertisement = ad;
    }
    public void run(){
        if(this.choice == 1)
            this.startService();
        else
            this.SendAdvertisement();
    }
    public void startService(){
        synchronized (NotificationMap)
        {
            try {
                Iterator<Map.Entry<Event,List<String>>> it = EventManager.NotificationMap.entrySet().iterator();
                while (it.hasNext()){
                    Map.Entry<Event,List<String>> mapElement = it.next();
                    Event e = mapElement.getKey();
                    List<String> agentIpPort = ((List<String>) mapElement.getValue());
                    List<String> namestoremove = new ArrayList<>();
                    for(String aip : agentIpPort)
                    {
                        String[] tempar = aip.split(",");
                        try {
                            Socket s = new Socket(tempar[0], Integer.parseInt(tempar[1]));
                            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                            dos.writeUTF(e.toString());
                            dos.close();
                            namestoremove.add(aip);
                        }
                        catch (IOException e2)
                        {
                            System.out.println(e2);
                            System.out.println("could not send..");
                        }
                    }
                    for(String name : namestoremove)
                    {
                        agentIpPort.remove((String)name);
                    }
                    if(agentIpPort.size() == 0)
                        it.remove();
                }
                System.out.println("done service");
            }catch (Exception e1)
            {
                System.out.println("exception in service");
                System.out.println(e1);
                e1.printStackTrace();
            }
        }
    }

    public void SendAdvertisement(){
        Event e = this.advertisement;
        List<String> agentsNotOnline = new ArrayList<>();
        synchronized (AgentIpPort) {
            for (Map.Entry mapElement : AgentIpPort.entrySet()) {
                String agentname = (String) mapElement.getKey();
                String agentIpPort = (String) mapElement.getValue();
                String[] tempar = agentIpPort.split(",");
                try {
                    Socket s = new Socket(tempar[0], Integer.parseInt(tempar[1]));
                    DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                    dos.writeUTF("new topic Ad ---" + e.toString());
                    dos.close();
                } catch (IOException e2) {
                    agentsNotOnline.add(agentIpPort);
                    System.out.println("could not send..");
                }
            }
        }
        synchronized (NotificationMap)
        {
            if(agentsNotOnline.size() > 0)
                NotificationMap.put(e,agentsNotOnline);
        }
    }

    public static void initDataStructures(){
        TopicMap.put(1,new Topic(1,"Sports"));TopicMap.put(2,new Topic(2,"News"));
        topicsSet.add(TopicMap.get(1));topicsSet.add(TopicMap.get(2));
        TopicAgentMap.put(1,new ArrayList<String>());TopicAgentMap.put(2,new ArrayList<String>());
    }


    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(4);
        initDataStructures();
        try{
            InetAddress inetAddress = InetAddress.getLocalHost();
            servsock = new ServerSocket(servport);
            System.out.println("Server serving on addr and port "+inetAddress.getHostAddress()+" "+servport);
            while (true)
            {
                Socket aclientsock = servsock.accept();
                System.out.println("one client sent....");
                pool.execute(new AgentHander(aclientsock));
            }
        } catch (IOException e) {
            System.out.println("IOException...in event manager");
            e.printStackTrace();
        }
    }
}


class AgentHander implements Runnable{
    Socket aclientsock;
    DataInputStream is;
    DataOutputStream os;
    String clientName;
    List<String> availableoptions = new ArrayList<String>(Arrays.asList("1","2","3"));
    public AgentHander(Socket aclientsock)
    {
        try {
            this.aclientsock = aclientsock;
            this.is = new DataInputStream(this.aclientsock.getInputStream());
            this.os = new DataOutputStream(this.aclientsock.getOutputStream());
        } catch (Exception e) {
            System.out.println("In agent handler contruct..."+e.getMessage());
            e.printStackTrace();
        }
    }
    public void run(){
        String options = "\n1-Publish \n2-Subscribe \n3-add new topic \n4-list subscribed topics \n5-List all topics" +
                " \n8-exit ";
        try {
            String regisdata = this.is.readUTF();
            if (this.CheckRegistered(regisdata)) {
                System.out.println("connected to "+this.clientName);
                this.os.writeUTF("registered....!" + options);
            } else {
                System.out.println("connected to "+this.clientName);
                this.os.writeUTF(options);
            }
            new EventManager(1).start();
            while (true) {
                String op = this.is.readUTF();
                switch (op)
                {
                    case "1":
                        this.os.writeUTF(this.getTopicIDs());
                        String inp = this.is.readUTF();
                        this.publish(inp);
                        this.os.writeUTF("published");
                        break;
                    case "2":
                        this.os.writeUTF("enter topic id's seperated by ',' ");
                        String topicids = this.is.readUTF();
                        boolean ans = this.addSubscriber(topicids);
                        this.os.writeUTF( ans ? "added" :"not added...exception occured");
                        break;
                    case "3":
                        this.os.writeUTF("enter topic name");
                        String adinp = this.is.readUTF();
                        this.os.writeUTF(this.Advertise(adinp) ? "added..sent for advertise" : "id already in use");
                        break;
                    case "4":
                        this.os.writeUTF(this.ListSubscribedTopics());
                        break;
                    case "5":
                        os.writeUTF(this.ListTopics());
                        break;
                    case "6":
                        this.printNotifs();
                        break;
                    case "7":
                        this.printTopicAgentMap();
                        break;
                    default:
                        throw new IOException();
                }
            }
        } catch (IOException e) {
            try {
                this.aclientsock.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.out.println("IOException in run..agent handler...client closed");
        }
    }

    public boolean Advertise(String adinp){
        Topic t;
        synchronized (EventManager.topicsSet)
        {
            synchronized (EventManager.GlobalTopicIDGen){
                Iterator it = EventManager.topicsSet.iterator();
                while(it.hasNext())
                {
                    if(it.next().toString().equals(adinp))
                        return false;
                }
                t = new Topic(EventManager.GlobalTopicIDGen,adinp);
                EventManager.GlobalTopicIDGen++;
                EventManager.topicsSet.add(t);
            }
        }
        synchronized (EventManager.TopicMap)
        {
            EventManager.TopicMap.put(t.id,t);
        }
        Event e;
        synchronized (EventManager.GlobalEventIDGen)
        {
            e = new Event(EventManager.GlobalEventIDGen,t," advertisement! new topic ",t.name);
            EventManager.GlobalEventIDGen++;
        }
        new EventManager(2,e).start();
        return true;
    }
    public boolean CheckRegistered(String regisdata){
        String[] info = regisdata.split(",");
        String name = info[0];
        this.clientName = name;
        String Ipaddr = info[1];
        String portnum = info[2];
        boolean ans = true;
        synchronized (EventManager.AgentIpPort)
        {
            if(EventManager.AgentIpPort.containsKey(name))
            {
                ans = false;
            }
            else{
                EventManager.AgentIpPort.put(name,info[1]+","+info[2]);
            }
        }
        synchronized (EventManager.ActiveAgents)
        {
            EventManager.ActiveAgents.put(name,true);
        }
        return ans;
    }

    public String ListTopics(){
        String temp = "";
        synchronized (EventManager.TopicMap){
            for (Map.Entry mapElement : EventManager.TopicMap.entrySet()) {
                Integer key = (Integer) mapElement.getKey();
                Topic value = ((Topic) mapElement.getValue());
                temp += key+"->"+value.name+"\n";
            }
        }
        return temp;
    }

    public boolean addSubscriber(String topicids){
        try {
            List<Topic> templis = new ArrayList<>();
            String[] topics = topicids.split(",");
            synchronized (EventManager.TopicMap)
            {
                for(String topic : topics)
                {
                    if(!EventManager.TopicMap.containsKey(Integer.parseInt(topic)))
                        continue;
                    else
                        templis.add(EventManager.TopicMap.get(Integer.parseInt(topic)));
                }
            }
            synchronized (EventManager.SubMap) {
                if (EventManager.SubMap.containsKey(this.clientName)) {
                    Set<Topic> clientTopicLis = EventManager.SubMap.get(this.clientName);
                    for (Topic topic : templis) {
                        clientTopicLis.add(topic);
                    }
                } else {
                    EventManager.SubMap.put(this.clientName, new HashSet<>());
                    Set<Topic> clientTopicLis = EventManager.SubMap.get(this.clientName);
                    for (Topic topic : templis) {
                        clientTopicLis.add(topic);
                    }
                }
            }
            synchronized (EventManager.TopicAgentMap){
                for (Topic t : templis)
                {
                    if(EventManager.TopicAgentMap.containsKey(t.id))
                    {
                        List<String> subs = EventManager.TopicAgentMap.get(t.id);
                        subs.add(this.clientName);
                    }
                    else{
                        List<String> subs = new ArrayList<>();subs.add(this.clientName);
                        EventManager.TopicAgentMap.put(t.id,subs);
                    }
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String ListSubscribedTopics(){
        List<Topic> templis = new ArrayList<>();
        String tempans = "";
        boolean ans = true;
        synchronized (EventManager.SubMap)
        {
            if(EventManager.SubMap.containsKey(this.clientName)) {
                Set<Topic> clientSubs = EventManager.SubMap.get(this.clientName);
                Iterator value = clientSubs.iterator();
                while (value.hasNext())
                    templis.add((Topic) value.next());
            }
            else {
                ans = false;
            }
        }
        if(ans==false)
            return "No subscriptions \n";

        for(Topic topic : templis)
        {
            tempans += topic.id+"->"+topic.name+"\n";
        }

        return tempans;
    }

    public String getTopicIDs(){
        String temp = "";
        synchronized (EventManager.TopicMap)
        {
            for(Integer id: EventManager.TopicMap.keySet())
                temp += id+",";
        }
        temp = temp.substring(0,temp.length()-1);
        return temp;
    }

    public void publish(String inp){
        try {
            String[] tempar = inp.split(",");
            Integer topicid = Integer.parseInt(tempar[0]);
            Topic currentTopic;
            synchronized (EventManager.TopicMap) {
                currentTopic = (Topic) EventManager.TopicMap.get(topicid);
            }
            String title = tempar[1];
            String content = tempar[2];
            Set<String> agentsToSend = new HashSet<>();
            synchronized (EventManager.TopicAgentMap)
            {
                if(EventManager.TopicAgentMap.get(currentTopic.id)!=null)
                    agentsToSend.addAll(EventManager.TopicAgentMap.get(currentTopic.id));
            }
            Set<String> activeagents = new HashSet<>();
            synchronized (EventManager.ActiveAgents)
            {
                for(String agentname : EventManager.ActiveAgents.keySet())
                {
                    if(EventManager.ActiveAgents.get(agentname))
                        activeagents.add(agentname);
                }
            }
            agentsToSend.retainAll(activeagents);
            System.out.println(agentsToSend);
            List<String> IpPorts = new ArrayList<>();
            synchronized (EventManager.AgentIpPort)
            {
                for(String agent : agentsToSend)
                    IpPorts.add(EventManager.AgentIpPort.get(agent)+","+agent);
            }
            System.out.println(IpPorts);
            List<String> bufferlist = new ArrayList<>();
            for(String agentIpport : IpPorts)
            {
                String[] ipport = agentIpport.split(",");
                try {
                    Socket s = new Socket(ipport[0], Integer.parseInt(ipport[1]));
                    DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                    dos.writeUTF("published by "+this.clientName+" : "+currentTopic.name+" -> "+title+" -> "+content);
                    s.close();
                }catch (IOException e) {
                    bufferlist.add(agentIpport);
                    System.out.println("not online...push to notif");
                }
            }
            if(bufferlist.size() > 0)
            {
                Event e;
                synchronized (EventManager.GlobalEventIDGen) {
                    e = new Event(EventManager.GlobalEventIDGen,currentTopic,title,content);
                    EventManager.GlobalEventIDGen++;
                }
                this.pushIntoNotifCache(bufferlist,e);
            }

        }
        catch (Exception e)
        {
            System.out.println("exception in publishing...");
            e.printStackTrace();
        }
    }

    public void pushIntoNotifCache(List<String> bufferlist,Event e)
    {
        synchronized (EventManager.NotificationMap)
        {
            EventManager.NotificationMap.put(e,bufferlist);
        }
    }

    public void printNotifs()
    {
        for (Map.Entry mapElement : EventManager.NotificationMap.entrySet()) {
            Event key = (Event) mapElement.getKey();
            List<String> value = ((List<String>) mapElement.getValue());
            System.out.println(key+" : "+value);
        }
    }
    public void printTopicAgentMap(){
        for (Map.Entry mapElement : EventManager.TopicAgentMap.entrySet()) {
            Integer key = (Integer)mapElement.getKey();
            List<String> value = ((List<String>) mapElement.getValue());
            System.out.println(key+" : "+value);
        }
    }
}