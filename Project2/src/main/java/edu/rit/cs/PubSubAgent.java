package edu.rit.cs;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.exit;
import static java.lang.System.out;

public class PubSubAgent implements Runnable{
    static String name;
    static InetAddress inetaddres;
    static String ipaddr;
    static int servport;
    static int portConnect;
    static String ServAddrr;
    static Socket s;
    static DataInputStream is;
    static DataOutputStream os;
    static Scanner in = new Scanner(System.in);
    public static void main(String[] args) throws IOException {
        try{
            name = args[0];
            ServAddrr = args[1];
            servport = Integer.parseInt(args[2]);
            inetaddres = InetAddress.getLocalHost();
            ipaddr = inetaddres.getHostAddress();
            System.out.println("client ip address is "+ipaddr);
            new Thread(new PubSubAgent()).start();
            s = new Socket(ServAddrr,9000);
            os = new DataOutputStream(s.getOutputStream());
            is = new DataInputStream(s.getInputStream());
            os.writeUTF(name+","+ipaddr+","+servport);
            String options = is.readUTF();
            options = options.substring(options.indexOf("\n")+1);
            while(true)
            {
                System.out.println(options);
                String option = in.nextLine();
                switch (option)
                {
                    case "1":
                        os.writeUTF(option);
                        String topicids = is.readUTF();
                        List<String> topicar = getTopicIDar(topicids);
                        os.writeUTF(getPublishInput(topicar));
                        out.println(is.readUTF());
                        break;
                    case "2":
                        os.writeUTF(option);
                        System.out.println(is.readUTF());
                        String topids = "";
                        while (true)
                        {
                            topids = in.nextLine();
                            if(checkTopicIDs(topids))
                            {
                                break;
                            }
                            else{
                                System.out.println("enter proper value");
                            }
                        }
                        os.writeUTF(topids);
                        System.out.println(is.readUTF());
                        break;
                    case "3":
                        os.writeUTF(option);
                        out.println(is.readUTF());
                        String inp = in.nextLine();
                        os.writeUTF(inp);
                        out.println(is.readUTF());
                        break;
                    case "4":
                        os.writeUTF(option);
                        out.println(" subscribed topics : ");
                        System.out.println(is.readUTF());
                        break;
                    case "5":
                        os.writeUTF(option);
                        System.out.println(is.readUTF());
                        break;
                    case "6":
                        os.writeUTF(option);
                        break;
                    case "7":
                        os.writeUTF(option);
                        break;
                    case "8":
                        throw new IOException();
                    default:
                        System.out.println("wrong option...enter again");
                }
            }
        } catch (UnknownHostException e) {
            System.out.println("  UnknownHostException.....  ");
            e.printStackTrace();
        } catch (IOException e) {
            s.close();
            System.out.println("IOException....");
            exit(0);
            e.printStackTrace();
        }
    }


    public static boolean checkTopicIDs(String inp)
    {
        try {
            if (inp.contains(",")) {
                String[] ids = inp.split(",");
                for (String id : ids) {
                    Integer temp = Integer.parseInt(id);
                }
                return true;
            } else {
                Integer temp = Integer.parseInt(inp);
                return true;
            }
        }catch (NumberFormatException e)
        {
            return false;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public static List<String> getTopicIDar(String topicids)
    {

        return Arrays.asList(topicids.split(","));
    }
    public static String getPublishInput(List<String> topicar)
    {
        System.out.println("enter in the format id,title,content");
        String inp;
        while (true)
        {
            inp = in.nextLine();
            boolean cond = true;
            String[] tempar = inp.split(",");
            if(tempar.length != 3)
            {
                System.out.println("enter proper value");cond=false;
            }
            if(!topicar.contains((String)tempar[0]))
            {
                System.out.println("enter proper value");cond=false;
            }
            if(cond)
                break;
            else
                continue;
        }
        return inp;
    }

    public void run(){
        try {
            ServerSocket serv = new ServerSocket(PubSubAgent.servport);
            out.println("serving on port..."+PubSubAgent.servport);
            while (true) {
                Socket s = serv.accept();
                new IncomingData(s).start();
            }
        }catch (IOException e)
        {
            out.println(e.getMessage());
        }
    }
}

class IncomingData extends Thread{
    Socket s;
    public IncomingData(Socket s)
    {
        this.s = s;
    }
    public void run(){
        try {
            DataInputStream dis = new DataInputStream(s.getInputStream());
            out.println(dis.readUTF());
            dis.close();
        }catch (IOException e)
        {
            out.println("exception in incoming data run...."+e.getMessage());
        }
    }
}