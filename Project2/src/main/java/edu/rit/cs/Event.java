package edu.rit.cs;

public class Event {
    private int id;
    private Topic topic;
    private String title;
    private String content;
    public Event(int id,Topic t,String title,String content)
    {
        this.id = id;this.topic = t;this.title = title;this.content = content;
    }
    public int getId()
    {
        return this.id;
    }
    public String getTitle(){
        return this.title;
    }
    public String getTopic(){
        return this.topic.toString();
    }
    public String getContent(){
        return this.content;
    }

    public String toString()
    {
        return this.topic.name+"->"+this.title+"->"+this.content;
    }
}
