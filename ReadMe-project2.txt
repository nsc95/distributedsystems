Hello!

Run the command 'docker-compose -f docker-compose-2.yml up --build' after entering into the distributedsystems
directory. This will create 3 containers where one can be used as the EventManager and the remaining as PubSub agents.

To create as many containers as you want...use the command
'docker run -it --network=distributedsystems_csci652network csci652:latest bash'

Once the containers are running, chose one as the server and use the command
'java -cp Project2/target/Project2-1.0-SNAPSHOT.jar edu.rit.cs.EventManager' to start the server...
Once the server starts, it displays the port (default: 9000) and ipaddr on which it is running...

To start a PubSubAgent, use the command
'java -cp Project2/target/Project2-1.0-SNAPSHOT.jar edu.rit.cs.PubSubAgent agent_name ipddr_of_server port_number_to_get_messages'

NOTE: DON'T ENTER SAME NAME FOR ANY TWO AGENTS, NAME IS USED AS KEY AND HENCE WILL GET OVERRIDEN, ALSO USE THE SAME PORT NUMBER
AS USED WHILE FIRST CONNECTING!

1st cmd arg : name_of_agent (please enter unique for each agent)
2nd cmd arg : ipaddr of server - displayed on server terminal when server is running..
3rd cmd arg : port number to get notifs for the agent.

Once the PubSubAgent sucessfully connects, it displays options to interact with...like:
1-publish
2-subscribe...
8-exit

NOTE: WHILE RECONNECTING TO THE SERVER, PLEASE USE THE SAME AGENT NAME AND PORT NUMBER AS USED DURING FIRST LOGIN.
